#ifndef _PICAXO_FILTER_H_
#define _PICAXO_FILTER_H_

#include <SDL2/SDL.h>

class Filter
{
public:
    Filter();
    ~Filter();

    virtual SDL_Surface * Apply(SDL_Surface * inputImage) = 0;

protected:
    virtual SDL_Surface * CreateSurface(int w, int h);
};

#endif // _PICAXO_FILTER_H_
