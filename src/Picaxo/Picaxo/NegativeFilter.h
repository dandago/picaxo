#ifndef _PICAXO_NEGATIVEFILTER_H_
#define _PICAXO_NEGATIVEFILTER_H_

#include <SDL2/SDL.h>

#include "Filter.h"

class NegativeFilter : public Filter
{
public:
    NegativeFilter();
    ~NegativeFilter();

    SDL_Surface * Apply(SDL_Surface * inputImage);
};

#endif // _PICAXO_NEGATIVEFILTER_H_