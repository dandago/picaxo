#ifndef _PICAXO_FLIPHORIZONTALFILTER_H_
#define _PICAXO_FLIPHORIZONTALFILTER_H_

#include <SDL2/SDL.h>

#include "Filter.h"

class FlipHorizontalFilter : public Filter
{
public:
    FlipHorizontalFilter();
    ~FlipHorizontalFilter();

    SDL_Surface * Apply(SDL_Surface * inputImage);
};

#endif // _PICAXO_FLIPHORIZONTALFILTER_H_