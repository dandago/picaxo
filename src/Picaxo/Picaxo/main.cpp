#include <iostream>

#include "ImageViewer.h"
#include "History.h"
#include "NegativeFilter.h"
#include "GrayscaleFilter.h"
#include "FlipHorizontalFilter.h"
#include "FlipVerticalFilter.h"
#include "RotateRightFilter.h"
#include "RotateLeftFilter.h"
#include "ConvolutionBlurFilter.h"
#include "ConvolutionSharpenFilter.h"
#include "ConvolutionMotionBlurFilter.h"
#include "ConvolutionEdgeFilter.h"
#include "ConvolutionEmbossFilter.h"
#include "ConvolutionMeanFilter.h"

using std::cout;
using std::endl;

bool HandleKeypress(ImageViewer &imageViewer, const SDL_Event &event)
{
    if (imageViewer.IsFullScreen())
        imageViewer.ToggleFullScreen();
    else
    {
        switch (event.key.keysym.sym)
        {
        case SDLK_g:
        {
            GrayscaleFilter filter;
            imageViewer.ApplyFilter(filter);
            break;
        }
        case SDLK_n:
        {
            NegativeFilter filter;
            imageViewer.ApplyFilter(filter);
            break;
        }
        case SDLK_h:
        {
            FlipHorizontalFilter filter;
            imageViewer.ApplyFilter(filter);
            break;
        }
        case SDLK_v:
        {
            FlipVerticalFilter filter;
            imageViewer.ApplyFilter(filter);
            break;
        }
        case SDLK_r:
        {
            RotateRightFilter filter;
            imageViewer.ApplyFilter(filter);
            break;
        }
        case SDLK_l:
        {
            RotateLeftFilter filter;
            imageViewer.ApplyFilter(filter);
            break;
        }
        case SDLK_b:
        {
            if (event.key.keysym.mod & KMOD_CTRL) // Ctrl+b -> motion blur filter
            {
                ConvolutionMotionBlurFilter filter;
                imageViewer.ApplyFilter(filter);
            }
            else // b -> blur filter
            {
                ConvolutionBlurFilter filter;
                imageViewer.ApplyFilter(filter);
            }
            break;
        }
        case SDLK_e:
        {
            if (event.key.keysym.mod & KMOD_CTRL) // Ctrl+e -> edge detection filter
            {
                ConvolutionEmbossFilter filter;
                imageViewer.ApplyFilter(filter);
            }
            else // e -> emboss filter
            {
                ConvolutionEdgeFilter filter;
                imageViewer.ApplyFilter(filter);
            }
            break;
        }
        case SDLK_m:
        {
            ConvolutionMeanFilter filter;
            imageViewer.ApplyFilter(filter);
            break;
        }
        case SDLK_p:
        {
            ConvolutionSharpenFilter filter;
            imageViewer.ApplyFilter(filter);
            break;
        }
        case SDLK_z:
            if (event.key.keysym.mod & KMOD_CTRL) // Ctrl+z -> undo
                imageViewer.Undo();
            break;
        case SDLK_y:
            if (event.key.keysym.mod & KMOD_CTRL) // Ctrl+y -> undo
                imageViewer.Redo();
            break;
        case SDLK_s:
            if (event.key.keysym.mod & KMOD_CTRL) // Ctrl+s -> Save BMP
                imageViewer.SaveBmp("picaxo_out.bmp");
            else // s -> Save PNG
                imageViewer.SavePng("picaxo_out.png");
            break;
        case SDLK_f:
            imageViewer.ToggleFullScreen();
            break;
        case SDLK_ESCAPE:
            return true;
        }
    }

    return false;
}

int main(int argc, char ** argv)
{
    if (argc > 1)
    {
        char * filename = argv[1];

        ImageViewer imageViewer;
        int init = imageViewer.Init(filename);

        if (init < 0)
            return init;

        SDL_Surface * image = imageViewer.GetImage();
        bool quit = false;
        SDL_Event event;

        while (!quit)
        {
            SDL_Delay(10); // keeps CPU usage down

            while (SDL_PollEvent(&event) != 0)
            {
                switch (event.type)
                {
                    case SDL_QUIT:
                        quit = true;
                        break;
                    case SDL_KEYDOWN:
                        quit = HandleKeypress(imageViewer, event);
                        break;
                    case SDL_WINDOWEVENT:
                        imageViewer.Refresh();
                        break;
                }
            }
        }

        return 0;
    }
    else
    {
        cout << "Usage: picaxo filename" << endl;
        return 1;
    }
}