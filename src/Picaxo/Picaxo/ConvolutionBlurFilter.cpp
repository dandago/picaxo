#include "ConvolutionBlurFilter.h"

ConvolutionBlurFilter::ConvolutionBlurFilter()
{
}


ConvolutionBlurFilter::~ConvolutionBlurFilter()
{
}

SDL_Surface * ConvolutionBlurFilter::Apply(SDL_Surface * inputImage)
{
    const int filterWidth = 3;
    const int filterHeight = 3;

    double filter[filterWidth * filterHeight] =
    {
        0,   0.2, 0,
        0.2, 0.2, 0.2,
        0,   0.2, 0
    };

    double factor = 1.0;
    double bias = 0.0;

    return this->ApplyConvolution(inputImage, filterWidth, filterHeight, filter, factor, bias);
}
