#include "FlipVerticalFilter.h"



FlipVerticalFilter::FlipVerticalFilter()
{
}


FlipVerticalFilter::~FlipVerticalFilter()
{
}

SDL_Surface * FlipVerticalFilter::Apply(SDL_Surface * inputImage)
{
    SDL_Surface * outputImage = this->CreateSurface(inputImage->w, inputImage->h);

    Uint32 * inputPixels = (Uint32 *)inputImage->pixels;
    Uint32 * outputPixels = (Uint32 *)outputImage->pixels;

    Uint32 topPixel = 0;
    Uint32 bottomPixel = 0;

    for (int y = 0; y < inputImage->h / 2; y++)
    {
        for (int x = 0; x < inputImage->w; x++)
        {
            topPixel = inputPixels[y * inputImage->w + x];
            bottomPixel = inputPixels[(inputImage->h - y - 1) * inputImage->w + x];

            outputPixels[y * inputImage->w + x] = bottomPixel;
            outputPixels[(inputImage->h - y - 1) * inputImage->w + x] = topPixel;
        }
    }

    return outputImage;
}