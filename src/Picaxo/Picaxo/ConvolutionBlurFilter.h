#ifndef _PICAXO_CONVOLUTIONBLURFILTER_H_
#define _PICAXO_CONVOLUTIONBLURFILTER_H_

#include "ConvolutionFilter.h"

class ConvolutionBlurFilter : public ConvolutionFilter
{
public:
    ConvolutionBlurFilter();
    ~ConvolutionBlurFilter();

    SDL_Surface * Apply(SDL_Surface * inputImage);
};

#endif // _PICAXO_CONVOLUTIONBLURFILTER_H_