#include "ConvolutionEmbossFilter.h"

ConvolutionEmbossFilter::ConvolutionEmbossFilter()
{
}

ConvolutionEmbossFilter::~ConvolutionEmbossFilter()
{
}

SDL_Surface * ConvolutionEmbossFilter::Apply(SDL_Surface * inputImage)
{
    const int filterWidth = 3;
    const int filterHeight = 3;

    double filter[filterWidth * filterHeight] =
    {
        -1, -1,  0,
        -1,  0,  1,
         0,  1,  1
    };

    double factor = 1.0;
    double bias = 128.0;

    return this->ApplyConvolution(inputImage, filterWidth, filterHeight, filter, factor, bias);
}