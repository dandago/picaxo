#ifndef _PICAXO_CONVOLUTIONEDGEFILTER_H_
#define _PICAXO_CONVOLUTIONEDGEFILTER_H_

#include "ConvolutionFilter.h"

class ConvolutionEdgeFilter : public ConvolutionFilter
{
public:
    ConvolutionEdgeFilter();
    ~ConvolutionEdgeFilter();

    SDL_Surface * Apply(SDL_Surface * inputImage);
};

#endif // _PICAXO_CONVOLUTIONEDGEFILTER_H_