#ifndef _PICAXO_ROTATELEFTFILTER_H_
#define _PICAXO_ROTATELEFTFILTER_H_

#include <SDL2/SDL.h>

#include "Filter.h"

class RotateLeftFilter : public Filter
{
public:
    RotateLeftFilter();
    ~RotateLeftFilter();

    SDL_Surface * Apply(SDL_Surface * inputImage);
};

#endif // _PICAXO_ROTATELEFTFILTER_H_