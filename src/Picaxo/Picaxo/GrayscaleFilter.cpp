#include "GrayscaleFilter.h"



GrayscaleFilter::GrayscaleFilter()
{
}


GrayscaleFilter::~GrayscaleFilter()
{
}

SDL_Surface * GrayscaleFilter::Apply(SDL_Surface * inputImage)
{
    SDL_Surface * outputImage = this->CreateSurface(inputImage->w, inputImage->h);

    Uint32 * inputPixels = (Uint32 *)inputImage->pixels;
    Uint32 * outputPixels = (Uint32 *)outputImage->pixels;

    for (int y = 0; y < inputImage->h; y++)
    {
        for (int x = 0; x < inputImage->w; x++)
        {
            Uint32 pixel = inputPixels[y * inputImage->w + x];

            Uint8 r = pixel >> 16 & 0xFF;
            Uint8 g = pixel >> 8 & 0xFF;
            Uint8 b = pixel & 0xFF;

            Uint8 v = (Uint8) (0.212671f * r + 0.715160f * g + 0.072169f * b);

            pixel = (0xFF << 24) | (v << 16) | (v << 8) | v;
            outputPixels[y * inputImage->w + x] = pixel;
        }
    }

    return outputImage;
}