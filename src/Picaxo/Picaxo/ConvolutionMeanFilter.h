#ifndef _PICAXO_CONVOLUTIONMEANFILTER_H_
#define _PICAXO_CONVOLUTIONMEANFILTER_H_

#include "ConvolutionFilter.h"

class ConvolutionMeanFilter : public ConvolutionFilter
{
public:
    ConvolutionMeanFilter();
    ~ConvolutionMeanFilter();

    SDL_Surface * Apply(SDL_Surface * inputImage);
};

#endif // _PICAXO_CONVOLUTIONMEANFILTER_H_