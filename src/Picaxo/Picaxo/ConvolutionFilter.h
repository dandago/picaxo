#ifndef _PICAXO_CONVOLUTIONFILTER_H_
#define _PICAXO_CONVOLUTIONFILTER_H_

#include "Filter.h"

class ConvolutionFilter : public Filter
{
public:
    ConvolutionFilter();
    ~ConvolutionFilter();

protected:
    SDL_Surface * ApplyConvolution(const SDL_Surface * inputImage,
        const int filterWidth, const int filterHeight, double * filter,
        const double factor, const double bias);
};

#endif // _PICAXO_CONVOLUTIONFILTER_H_
