#ifndef _PICAXO_GRAYSCALEFILTER_H_
#define _PICAXO_GRAYSCALEFILTER_H_

#include <SDL2/SDL.h>

#include "Filter.h"

class GrayscaleFilter : public Filter
{
public:
    GrayscaleFilter();
    ~GrayscaleFilter();

    SDL_Surface * Apply(SDL_Surface * inputImage);
};

#endif // _PICAXO_GRAYSCALEFILTER_H_