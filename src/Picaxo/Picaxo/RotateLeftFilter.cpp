#include "RotateLeftFilter.h"



RotateLeftFilter::RotateLeftFilter()
{
}


RotateLeftFilter::~RotateLeftFilter()
{
}

SDL_Surface * RotateLeftFilter::Apply(SDL_Surface * inputImage)
{
    int newWidth = inputImage->h;
    int newHeight = inputImage->w;
    SDL_Surface * outputImage = this->CreateSurface(newWidth, newHeight);

    Uint32 * inputPixels = (Uint32 *)inputImage->pixels;
    Uint32 * outputPixels = (Uint32 *)outputImage->pixels;

    Uint32 topPixel = 0;
    Uint32 bottomPixel = 0;

    for (int y = 0; y < inputImage->h; y++)
    {
        for (int x = 0; x < inputImage->w; x++)
        {
            int newX = y;
            int newY = inputImage->w - 1 - x;

            Uint32 inputPixel = inputPixels[y * inputImage->w + x];
            outputPixels[newY * outputImage->w + newX] = inputPixel;
        }
    }

    return outputImage;
}
