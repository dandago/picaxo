#ifndef _PICAXO_CONVOLUTIONEMBOSSFILTER_H_
#define _PICAXO_CONVOLUTIONEMBOSSFILTER_H_

#include "ConvolutionFilter.h"

class ConvolutionEmbossFilter : public ConvolutionFilter
{
public:
    ConvolutionEmbossFilter();
    ~ConvolutionEmbossFilter();

    SDL_Surface * Apply(SDL_Surface * inputImage);
};

#endif // _PICAXO_CONVOLUTIONEMBOSSFILTER_H_