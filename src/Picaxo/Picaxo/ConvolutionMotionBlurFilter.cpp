#include "ConvolutionMotionBlurFilter.h"

ConvolutionMotionBlurFilter::ConvolutionMotionBlurFilter()
{
}

ConvolutionMotionBlurFilter::~ConvolutionMotionBlurFilter()
{
}

SDL_Surface * ConvolutionMotionBlurFilter::Apply(SDL_Surface * inputImage)
{
    const int filterWidth = 9;
    const int filterHeight = 9;

    double filter[filterWidth * filterHeight] =
    {
        1, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 1, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 1, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 1, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 1, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 1, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 1, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 1, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 1,
    };

    double factor = 1.0 / 9.0;
    double bias = 0.0;

    return this->ApplyConvolution(inputImage, filterWidth, filterHeight, filter, factor, bias);
}
