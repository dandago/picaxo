#ifndef _PICAXO_IMAGEVIEWER_H_
#define _PICAXO_IMAGEVIEWER_H_

#include <SDL2/SDL.h>

#include "History.h"
#include "Filter.h"

class ImageViewer
{
public:
    ImageViewer();
    ~ImageViewer();

    int Init(const char * filePath);
    SDL_Surface * GetImage();
    int GetWindowWidth();
    int GetWindowHeight();
    void ApplyFilter(Filter &filter);
    void Undo();
    void Redo();
    const void SaveBmp(const char * filename);
    const void SavePng(const char * filename);
    void ToggleFullScreen();
    const bool IsFullScreen();
    const void Refresh();

private:
    SDL_Window * window = nullptr;
    SDL_Surface * image = nullptr;
    SDL_Renderer * renderer = nullptr;
    SDL_Texture * texture = nullptr;
    History history;
    bool fullScreen;

    int SetImage(SDL_Surface * image);
    const void UpdateTexture(SDL_Rect * dstrect);
    const void CalculateWindowSize(SDL_Surface * image, SDL_Rect &rect);
};

#endif // _PICAXO_IMAGEVIEWER_H_