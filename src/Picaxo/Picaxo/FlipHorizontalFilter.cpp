#include "FlipHorizontalFilter.h"



FlipHorizontalFilter::FlipHorizontalFilter()
{
}


FlipHorizontalFilter::~FlipHorizontalFilter()
{
}

SDL_Surface * FlipHorizontalFilter::Apply(SDL_Surface * inputImage)
{
    SDL_Surface * outputImage = this->CreateSurface(inputImage->w, inputImage->h);

    Uint32 * inputPixels = (Uint32 *)inputImage->pixels;
    Uint32 * outputPixels = (Uint32 *)outputImage->pixels;

    Uint32 leftPixel = 0;
    Uint32 rightPixel = 0;

    for (int y = 0; y < inputImage->h; y++)
    {
        for (int x = 0; x < inputImage->w / 2; x++)
        {
            leftPixel = inputPixels[y * inputImage->w + x];
            rightPixel = inputPixels[y * inputImage->w + (inputImage->w - x - 1)];

            outputPixels[y * inputImage->w + x] = rightPixel;
            outputPixels[y * inputImage->w + (inputImage->w - x - 1)] = leftPixel;
        }
    }

    return outputImage;
}