#include <algorithm>

#include <SDL2/SDL_image.h>

#include "ImageViewer.h"
#include "Filter.h"

using std::max;
using std::min;
using std::swap;

ImageViewer::ImageViewer()
    : fullScreen(false)
{

}

int ImageViewer::Init(const char * filePath)
{
    SDL_Init(SDL_INIT_VIDEO);

    IMG_Init(IMG_INIT_PNG | IMG_INIT_JPG | IMG_INIT_TIF);
    SDL_Surface * image = IMG_Load(filePath);
    SDL_Surface * icon = IMG_Load("pcxico32.png");
    IMG_Quit();

    if (image == NULL)
    {
        SDL_ShowSimpleMessageBox(0, "Image init error", SDL_GetError(),
            NULL);
        return -1;
    }

    SDL_Rect windowSize;
    this->CalculateWindowSize(image, windowSize);

    this->window = SDL_CreateWindow("Picaxo 2",
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, windowSize.w, windowSize.h, 0);

    if (icon != NULL)
        SDL_SetWindowIcon(this->window, icon);
    SDL_FreeSurface(icon);

    this->renderer = SDL_CreateRenderer(this->window, -1, 0);

    bool result = this->SetImage(image);
    this->history.Add(this->image); // add image to history the first time

    SDL_FreeSurface(image);

    return result;
}

SDL_Surface * ImageViewer::GetImage()
{
    return this->image;
}

int ImageViewer::SetImage(SDL_Surface * image)
{
    // store old dimensions to see if a window resize is necessary

    int oldImageW = this->image == NULL ? -1 : this->image->w;
    int oldImageH = this->image == NULL ? -1 : this->image->h;

    // free old resources

    if (this->texture != NULL)
        SDL_DestroyTexture(this->texture);

    // validate image dimensions against texture maximum

    SDL_RendererInfo rendererInfo;
    SDL_GetRendererInfo(this->renderer, &rendererInfo);

    if (image->w > rendererInfo.max_texture_width || image->h > rendererInfo.max_texture_height)
    {
        SDL_ShowSimpleMessageBox(0, "Image too large", "Image exceeds maximum texture dimensions", NULL);
        return -1;
    }

    // create texture

    this->texture = SDL_CreateTexture(renderer,
        SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STATIC,
        image->w, image->h);

    if (this->texture == NULL)
    {
        SDL_ShowSimpleMessageBox(0, "Texture init error", SDL_GetError(), NULL);
        return -1;
    }

    // convert surface to common format

    this->image = SDL_ConvertSurfaceFormat(image, SDL_PIXELFORMAT_ARGB8888, 0);

    if (this->image == NULL)
    {
        SDL_ShowSimpleMessageBox(0, "Surface convert error", SDL_GetError(), NULL);
        return -1;
    }

    // update texture

    int updateResult = SDL_UpdateTexture(this->texture, NULL, this->image->pixels, this->image->w * sizeof(Uint32));

    if (updateResult != 0)
    {
        SDL_ShowSimpleMessageBox(0, "Texture update error", SDL_GetError(), NULL);
        return -1;
    }

    // resize window if necessary

    if (oldImageW != this->image->w || oldImageH != this->image->h)
    {
        SDL_Rect windowSize;
        this->CalculateWindowSize(this->image, windowSize);

        SDL_SetWindowSize(this->window, windowSize.w, windowSize.h);
    }

    // update texture

    this->Refresh();

    return 0;
}

ImageViewer::~ImageViewer()
{
    SDL_DestroyTexture(texture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);

    SDL_Quit();
}

int ImageViewer::GetWindowWidth()
{
    return this->image->w;
}

int ImageViewer::GetWindowHeight()
{
    return this->image->h;
}

void ImageViewer::ApplyFilter(Filter &filter)
{
    if (this->image != NULL)
    {
        SDL_Surface * oldImage = this->image;
        SDL_Surface * newImage = filter.Apply(oldImage);
        this->SetImage(newImage);
        this->history.Add(this->image); // add image to history when a filter is applied
    }
}

void ImageViewer::Undo()
{
    SDL_Surface * previousImage = this->history.Undo();
    if (previousImage != NULL)
        this->SetImage(previousImage);
}

void ImageViewer::Redo()
{
    SDL_Surface * nextImage = this->history.Redo();
    if (nextImage != NULL)
        this->SetImage(nextImage);
}

const void ImageViewer::SaveBmp(const char * filename)
{
    SDL_SaveBMP(this->image, filename);
}

const void ImageViewer::SavePng(const char * filename)
{
    IMG_Init(IMG_INIT_PNG);
    IMG_SavePNG(this->image, filename);
    IMG_Quit();
}

void ImageViewer::ToggleFullScreen()
{
    Uint32 flags = this->fullScreen ? 0 : SDL_WINDOW_FULLSCREEN_DESKTOP;
    SDL_SetWindowFullscreen(this->window, flags);
    this->fullScreen = !(this->fullScreen);
}

const bool ImageViewer::IsFullScreen()
{
    return this->fullScreen;
}

const void ImageViewer::Refresh()
{
    if (this->image->w > 200 && this->image->h > 200)
    {
        this->UpdateTexture(NULL);
    }
    else
    {
        // if image is too small, pad it

        SDL_Rect dstrect = {
            (200 - this->image->w) / 2,
            (200 - this->image->h) / 2,
            this->image->w,
            this->image->h
        };

        this->UpdateTexture(&dstrect);
    }
}

const void ImageViewer::UpdateTexture(SDL_Rect * dstrect)
{
    SDL_UpdateTexture(texture, NULL, this->image->pixels, this->image->w * sizeof(Uint32));
    SDL_RenderCopy(renderer, texture, NULL, dstrect);
    SDL_RenderPresent(renderer);
}

const void ImageViewer::CalculateWindowSize(SDL_Surface * image, SDL_Rect &rect)
{
    SDL_DisplayMode currentDisplay;
    const int primaryScreenIndex = 0;
    SDL_GetCurrentDisplayMode(primaryScreenIndex, &currentDisplay);

    int screenW = currentDisplay.w - 80;
    int screenH = currentDisplay.h - 150;

    int imageW = max(image->w, 200);
    int imageH = max(image->h, 200);

    float oldAspectRatio = (float)imageW / imageH;

    int windowW = min(screenW, imageW);
    int windowH = min(screenH, imageH);

    float newAspectRatio = (float)windowW / windowH;

    if (newAspectRatio > oldAspectRatio)
        windowW = (int)(oldAspectRatio / newAspectRatio * (float)windowW);
    else
        windowH = (int)(newAspectRatio / oldAspectRatio * (float)windowH);

    rect.w = windowW;
    rect.h = windowH;
}
