#ifndef _PICAXO_CONVOLUTIONSHARPENFILTER_H_
#define _PICAXO_CONVOLUTIONSHARPENFILTER_H_

#include "ConvolutionFilter.h"
class ConvolutionSharpenFilter :
    public ConvolutionFilter
{
public:
    ConvolutionSharpenFilter();
    ~ConvolutionSharpenFilter();

    SDL_Surface * Apply(SDL_Surface * inputImage);
};

#endif // _PICAXO_CONVOLUTIONSHARPENFILTER_H_