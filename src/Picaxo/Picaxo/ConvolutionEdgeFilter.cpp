#include "ConvolutionEdgeFilter.h"

ConvolutionEdgeFilter::ConvolutionEdgeFilter()
{
}

ConvolutionEdgeFilter::~ConvolutionEdgeFilter()
{
}

SDL_Surface * ConvolutionEdgeFilter::Apply(SDL_Surface * inputImage)
{
    const int filterWidth = 3;
    const int filterHeight = 3;

    double filter[filterWidth * filterHeight] =
    {
        -1, -1, -1
        - 1,  8, -1
        - 1, -1, -1
    };

    double factor = 1.0;
    double bias = 0.0;

    return this->ApplyConvolution(inputImage, filterWidth, filterHeight, filter, factor, bias);
}