#ifndef _PICAXO_ROTATERIGHTFILTER_H_
#define _PICAXO_ROTATERIGHTFILTER_H_

#include <SDL2/SDL.h>

#include "Filter.h"

class RotateRightFilter : public Filter
{
public:
    RotateRightFilter();
    ~RotateRightFilter();

    SDL_Surface * Apply(SDL_Surface * inputImage);
};

#endif // _PICAXO_ROTATERIGHTFILTER_H_