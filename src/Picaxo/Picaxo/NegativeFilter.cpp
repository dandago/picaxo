#include "NegativeFilter.h"

NegativeFilter::NegativeFilter()
{
}


NegativeFilter::~NegativeFilter()
{
}

SDL_Surface * NegativeFilter::Apply(SDL_Surface * inputImage)
{
    SDL_Surface * outputImage = this->CreateSurface(inputImage->w, inputImage->h);

    Uint32 * inputPixels = (Uint32 *) inputImage->pixels;
    Uint32 * outputPixels = (Uint32 *) outputImage->pixels;

    for (int y = 0; y < inputImage->h; y++)
    {
        for (int x = 0; x < inputImage->w; x++)
        {
            Uint32 pixel = inputPixels[y * inputImage->w + x];

            Uint8 r = pixel >> 16 & 0xFF;
            Uint8 g = pixel >> 8 & 0xFF;
            Uint8 b = pixel & 0xFF;

            r = 255 - r;
            g = 255 - g;
            b = 255 - b;

            pixel = (0xFF << 24) | (r << 16) | (g << 8) | b;
            outputPixels[y * inputImage->w + x] = pixel;
        }
    }

    return outputImage;
}
