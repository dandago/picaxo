#include <algorithm>

#include "ConvolutionFilter.h"

using std::min;
using std::max;

ConvolutionFilter::ConvolutionFilter()
{
}


ConvolutionFilter::~ConvolutionFilter()
{
}

SDL_Surface * ConvolutionFilter::ApplyConvolution(const SDL_Surface * inputImage,
    const int filterWidth, const int filterHeight, double * filter,
    const double factor, const double bias)
{
    int w = inputImage->w;
    int h = inputImage->h;

    SDL_Surface * outputImage = this->CreateSurface(w, h);

    Uint32 * inputPixels = (Uint32 *)inputImage->pixels;
    Uint32 * outputPixels = (Uint32 *)outputImage->pixels;

    for (int y = 0; y < inputImage->h; y++)
    for (int x = 0; x < inputImage->w; x++)
    {
        double r = 0.0;
        double g = 0.0;
        double b = 0.0;

        for (int filterY = 0; filterY < filterHeight; filterY++)
        for (int filterX = 0; filterX < filterWidth; filterX++)
        {
            int imageX = (x - filterWidth / 2 + filterX + w) % w;
            int imageY = (y - filterHeight / 2 + filterY + h) % h;

            Uint32 pixel = inputPixels[imageY * w + imageX];

            int pixelRed = pixel >> 16 & 0xFF;
            int pixelGreen = pixel >> 8 & 0xFF;
            int pixelBlue = pixel & 0xFF;

            r += pixelRed * filter[filterY * filterWidth + filterX];
            g += pixelGreen * filter[filterY * filterWidth + filterX];
            b += pixelBlue * filter[filterY * filterWidth + filterX];
        }

        r = min(max(int(factor * r + bias), 0), 255);
        g = min(max(int(factor * g + bias), 0), 255);
        b = min(max(int(factor * b + bias), 0), 255);

        Uint32 pixel = (0xFF << 24) | ((int)r << 16) | ((int)g << 8) | (int)b;
        outputPixels[y * w + x] = pixel;
    }

    return outputImage;
}
