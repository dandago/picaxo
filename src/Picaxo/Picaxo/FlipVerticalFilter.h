#ifndef _PICAXO_FLIPVERTICALFILTER_H_
#define _PICAXO_FLIPVERTICALFILTER_H_

#include <SDL2/SDL.h>

#include "Filter.h"

class FlipVerticalFilter : public Filter
{
public:
    FlipVerticalFilter();
    ~FlipVerticalFilter();

    SDL_Surface * Apply(SDL_Surface * inputImage);
};

#endif // _PICAXO_FLIPVERTICALFILTER_H_