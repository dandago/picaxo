#ifndef _PICAXO_CONVOLUTIONMOTIONBLURFILTER_H_
#define _PICAXO_CONVOLUTIONMOTIONBLURFILTER_H_

#include "ConvolutionFilter.h"
class ConvolutionMotionBlurFilter : public ConvolutionFilter
{
public:
    ConvolutionMotionBlurFilter();
    ~ConvolutionMotionBlurFilter();

    SDL_Surface * Apply(SDL_Surface * inputImage);
};

#endif // _PICAXO_CONVOLUTIONMOTIONBLURFILTER_H_