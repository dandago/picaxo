#ifndef _PICAXO_HISTORY_H_
#define _PICAXO_HISTORY_H_

#include <SDL2/SDL.h>

struct HistoryItem
{
    HistoryItem * prev;
    HistoryItem * next;
    SDL_Surface * image;
};

class History
{
public:
    History();
    ~History();

    void Add(SDL_Surface * image);
    SDL_Surface * Undo();
    SDL_Surface * Redo();

private:
    HistoryItem * currentHistoryItem;
};

#endif // _PICAXO_HISTORY_H_