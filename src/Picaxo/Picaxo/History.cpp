#include <SDL2/SDL.h>

#include "History.h"

History::History()
    : currentHistoryItem(NULL)
{

}

History::~History()
{
    if (currentHistoryItem != NULL)
    {
        // rewind to first element

        while (currentHistoryItem->prev != NULL)
            currentHistoryItem = currentHistoryItem->prev;

        // iterate through each element, freeing resources

        while (currentHistoryItem != NULL)
        {
            SDL_FreeSurface(currentHistoryItem->image);
            currentHistoryItem = currentHistoryItem->next;
        }
    }
}

void History::Add(SDL_Surface * image)
{
    HistoryItem * newHistoryItem = new HistoryItem;
    newHistoryItem->image = image;
    newHistoryItem->prev = currentHistoryItem;
    newHistoryItem->next = NULL;

    if (currentHistoryItem != NULL)
    {
        // adding a history entry in the middle of the history
        // causes any subsequent entries to be deleted

        HistoryItem * delHistoryItem = currentHistoryItem->next;
        while (delHistoryItem != NULL)
        {
            HistoryItem * next = delHistoryItem->next;
            delete delHistoryItem;
            delHistoryItem = next;
        }

        // update next pointer

        this->currentHistoryItem->next = newHistoryItem;
    }
    
    // set new history item

    this->currentHistoryItem = newHistoryItem;
}

SDL_Surface * History::Undo()
{
    if (this->currentHistoryItem->prev != NULL)
    {
        this->currentHistoryItem = this->currentHistoryItem->prev;
        return this->currentHistoryItem->image;
    }
    else
        return NULL;
}

SDL_Surface * History::Redo()
{
    if (this->currentHistoryItem->next != NULL)
    {
        this->currentHistoryItem = this->currentHistoryItem->next;
        return this->currentHistoryItem->image;
    }
    else
        return NULL;
}